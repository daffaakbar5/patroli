import Layout from "@/components/layout";
import checkAuth from "@/middlewares/auth";
import React, { useEffect, useState } from "react";
import { saveAs } from "file-saver";
import API from "@/config/api";
import { jwtDecode } from "jwt-decode";
import ReactPaginate from "react-paginate";
import { useRouter } from "next/router";

const HomeUser = () => {
  const [username, setUsername] = useState("");
  const [searchTerm, setSearchTerm] = useState("");
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [searchResults, setSearchResults] = useState([]);
  const [dataLaporan, setDataLaporan] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [role, setRole] = useState("");
  const [currentPage, setCurrentPage] = useState(0);
  const itemsPerPage = 10;
  const navigate = useRouter();
  useEffect(() => {
    fetchLaporan();
    const token = localStorage.getItem("token");
    const decodedToken = jwtDecode(token);
    const role = decodedToken.role;
    setUsername(decodedToken.username);
    setRole(role);
  }, []);

  const fetchLaporan = async () => {
    setIsLoading(true);
    const token = localStorage.getItem("token");
    const decodedToken = jwtDecode(token);
    const id = decodedToken.id;
    const role = decodedToken.role;
    try {
      let response;
      if (role === "admin") {
        response = await API.get("/patrols", {
          headers: {
            Authorization: token,
          },
        });
      } else {
        response = await API.get(`/patrols/by-security/${id}`, {
          headers: {
            Authorization: token,
          },
        });
      }
      setDataLaporan(response.data.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    const filteredResults = dataLaporan.filter(
      (row) =>
        (row.area_name.toLowerCase().includes(searchTerm.toLowerCase()) ||
          row.name.toLowerCase().includes(searchTerm.toLowerCase())) &&
        (!startDate || new Date(row.timestamp) >= new Date(startDate)) &&
        (!endDate || new Date(row.timestamp) <= new Date(endDate))
    );
    setSearchResults(filteredResults);
  }, [searchTerm, startDate, endDate, dataLaporan]);

  const convertToCSV = () => {
    const csvData = [["#", "Area", "Security", "Waktu"]];
    searchResults.forEach((row, index) => {
      const formattedTimestamp = new Date(row.timestamp)
        .toLocaleString("en-US", {
          year: "numeric",
          month: "2-digit",
          day: "2-digit",
          hour: "2-digit",
          minute: "2-digit",
          second: "2-digit",
          hour12: true,
        })
        .replace(/,/g, "");
      const rowData = [index + 1, row.area_name, row.name, formattedTimestamp];
      csvData.push(rowData);
    });
    const csvContent = csvData.map((row) => row.join(",")).join("\n");
    return csvContent;
  };

  const handleDownload = () => {
    const csvContent = convertToCSV();
    const blob = new Blob([csvContent], { type: "text/csv;charset=utf-8" });
    saveAs(blob, "riwayat_patroli.csv");
  };

  const onPageChange = ({ selected }) => setCurrentPage(selected);

  const offset = currentPage * itemsPerPage;
  const pageCount = Math.ceil(searchResults.length / itemsPerPage);
  const currentData = searchResults.slice(offset, offset + itemsPerPage);

  return (
    <Layout>
      {role === "security" && (
        <h1 className="sm:text-left mt-4 text-base">
          Hai <span className="font-medium">{username}</span>
        </h1>
      )}
      <h1 className="font-bold text-xl uppercase">Laporan Patroli</h1>
      <div className="my-4">
        {role === "admin" && (
          <div className="flex flex-col md:flex-row mb-4 md:gap-4">
            <label className="input input-bordered flex items-center gap-2">
              <input
                type="text"
                className="grow"
                placeholder="Search"
                value={searchTerm}
                onChange={(e) => setSearchTerm(e.target.value)}
              />
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 16 16"
                fill="currentColor"
                className="w-4 h-4 md:w-auto md:h-auto opacity-70"
              >
                <path
                  fillRule="evenodd"
                  d="M9.965 11.026a5 5 0 1 1 1.06-1.06l2.755 2.754a.75.75 0 1 1-1.06 1.06l-2.755-2.754ZM10.5 7a3.5 3.5 0 1 1-7 0 3.5 3.5 0 0 1 7 0Z"
                  clipRule="evenodd"
                />
              </svg>
            </label>
            <div className="flex flex-col md:flex-row md:gap-4">
              <label className="input input-bordered flex items-center gap-2">
                <input
                  type="date"
                  className="grow"
                  placeholder="Start Date"
                  value={startDate}
                  onChange={(e) => setStartDate(e.target.value)}
                />
              </label>
              <label className="input input-bordered flex items-center gap-2">
                <input
                  type="date"
                  className="grow"
                  placeholder="End Date"
                  value={endDate}
                  onChange={(e) => setEndDate(e.target.value)}
                />
              </label>
            </div>
            <button
              className="btn btn-primary mt-2 md:mt-0"
              onClick={handleDownload}
            >
              Download CSV
            </button>
          </div>
        )}
        {isLoading && (
          <div className="flex w-full justify-center">
            <span className="loading loading-infinity loading-lg"></span>
          </div>
        )}
        <div className="overflow-x-auto my-4 z-0">
          <table className="table">
            <thead>
              <tr>
                <th>#</th>
                <th>Area</th>
                <th>Security</th>
                <th>Waktu</th>
              </tr>
            </thead>
            <tbody>
              {currentData.map((row, index) => (
                <tr key={index}>
                  <td>{index + 1 + offset}</td>
                  <td>{row.area_name}</td>
                  <td>{row.name}</td>
                  <td>{row.timestamp}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        {dataLaporan.length >= 10 && (
          <div className="join flex justify-center">
            <ReactPaginate
              previousLabel={
                <button className="join-item btn btn-outline">
                  Previous page
                </button>
              }
              nextLabel={
                <button className="join-item btn btn-outline">Next</button>
              }
              pageCount={pageCount}
              onPageChange={onPageChange}
              containerClassName={"join"}
              pageClassName={"join-item btn"}
              previousLinkClassName={"pagination__link"}
              nextLinkClassName={"pagination__link"}
              disabledClassName={"pagination__link--disabled"}
              activeClassName={"pagination__link--active"}
            />
          </div>
        )}
      </div>
      {role === "security" && (
        <button
          className="btn w-16 h-16 btn-circle btn-primary fixed bottom-5 z-50 right-5"
          onClick={() => navigate.push("scan-barcode")}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="40"
            height="40"
            fill="currentColor"
            class="bi bi-camera"
            viewBox="0 0 16 16"
          >
            <path d="M15 12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V6a1 1 0 0 1 1-1h1.172a3 3 0 0 0 2.12-.879l.83-.828A1 1 0 0 1 6.827 3h2.344a1 1 0 0 1 .707.293l.828.828A3 3 0 0 0 12.828 5H14a1 1 0 0 1 1 1zM2 4a2 2 0 0 0-2 2v6a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2h-1.172a2 2 0 0 1-1.414-.586l-.828-.828A2 2 0 0 0 9.172 2H6.828a2 2 0 0 0-1.414.586l-.828.828A2 2 0 0 1 3.172 4z" />
            <path d="M8 11a2.5 2.5 0 1 1 0-5 2.5 2.5 0 0 1 0 5m0 1a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7M3 6.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0" />
          </svg>
        </button>
      )}
    </Layout>
  );
};

export default checkAuth(HomeUser);
