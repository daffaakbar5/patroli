import Image from "next/image";
import { useRouter } from "next/router";
import API from "@/config/api";
import { useState } from "react";
import Swal from "sweetalert2";
import checkAuth from "@/middlewares/auth";
import { jwtDecode } from "jwt-decode";

const Home = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const navigate = useRouter();

  const handleLogin = async () => {
    try {
      setIsLoading(true);
      await API.post("/users/login", {
        username,
        password,
      })
        .then((data) => {
          const { token } = data.data;
          // Simpan token ke penyimpanan lokal (misalnya localStorage atau sessionStorage)
          Swal.fire({ title: "Login  Berhasil", icon: "success" });
          localStorage.setItem("token", token); // Simpan token di localStorage
          setIsLoading(false);
          navigate.push("/home");
        })
        .catch((error) =>
          Swal.fire({ title: error.response.data.message, icon: "error" })
        );
    } catch (error) {
      if (error.response) {
        Swal.fire({ title: error.response.data.message, icon: "error" });
        // Tanggapan gagal atau kesalahan jaringan
      }
      console.error("Login gagal:", error);
    }
    setIsLoading(false);
  };
  return (
    <div className="min-h-screen bg-gray-100 flex flex-col justify-center sm:py-12">
      <div className="p-10 xs:p-0 mx-auto md:w-full md:max-w-md">
        {/* <h1 className="font-bold text-center text-2xl mb-5">Your Logo</h1> */}
        <Image
          src={"/logo-sarana.png"}
          width={200}
          height={100}
          alt="Logo"
          className=" text-center bg-red-600 p-2 mb-4 mx-auto"
        />
        <div className="bg-white shadow w-full rounded-lg divide-y divide-gray-200">
          <div className="px-5 py-7">
            <label className="font-semibold text-sm text-gray-600 pb-1 block">
              Username
            </label>
            <input
              type="text"
              className="border rounded-lg px-3 py-2 mt-1 mb-5 text-sm w-full"
              onChange={(e) => setUsername(e.target.value)}
            />
            <label className="font-semibold text-sm text-gray-600 pb-1 block">
              Password
            </label>
            <input
              type="password"
              className="border rounded-lg px-3 py-2 mt-1 mb-5 text-sm w-full"
              onChange={(e) => setPassword(e.target.value)}
            />
            {isLoading ? (
              <button type="button" className=" btn btn-primary w-full ">
                <span className="loading loading-spinner"></span>
                loading
              </button>
            ) : (
              <button
                type="button"
                className=" btn btn-primary w-full text-white"
                onClick={handleLogin}
                // onClick={() => navigate.push("/home")}
              >
                Login
              </button>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};
export default checkAuth(Home);
