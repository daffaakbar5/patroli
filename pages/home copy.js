import Layout from "@/components/layout";
import checkAuth from "@/middlewares/auth";
import React, { useEffect, useState } from "react";
import { saveAs } from "file-saver";
import API from "@/config/api";
import { jwtDecode } from "jwt-decode";

const HomeUser = () => {
  const [username, setUsername] = useState("");
  const [searchTerm, setSearchTerm] = useState("");
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [searchResults, setSearchResults] = useState([]);
  const [dataLaporan, setDataLaporan] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [role, setRole] = useState("");

  useEffect(() => {
    fetchLaporan();
    const token = localStorage.getItem("token");
    const decodedToken = jwtDecode(token);
    const role = decodedToken.role;
    // console.log("dee", decodedToken);
    setUsername(decodedToken.username);

    setRole(role);
  }, []);
  const fetchLaporan = async () => {
    setIsLoading(true);
    const token = localStorage.getItem("token");
    const decodedToken = jwtDecode(token);
    const id = decodedToken.id;
    const role = decodedToken.role;
    if (role === "admin") {
      await API.get("/patrols", {
        headers: {
          Authorization: `${token}`, // Sertakan token dalam header Authorization
        },
      })
        .then((data) => {
          console.log("DATA=>", data.data.data);
          setDataLaporan(data.data.data);
        })
        .catch((err) => console.log("Err", err));
    } else {
      await API.get(`/patrols/by-security/${id}`, {
        headers: {
          Authorization: `${token}`, // Sertakan token dalam header Authorization
        },
      })
        .then((data) => {
          console.log("DATA=>", data.data.data);
          setDataLaporan(data.data.data);
        })
        .catch((err) => console.log("Err", err));
    }
    setIsLoading(false);
  };
  useEffect(() => {
    console.log("dataLaporan", dataLaporan);
    const results = dataLaporan
      .filter(
        (row) =>
          row.area_name.toLowerCase().includes(searchTerm.toLowerCase()) ||
          row.name.toLowerCase().includes(searchTerm.toLowerCase())
      )
      .filter(
        (row) =>
          (!startDate || new Date(row.timestamp) >= new Date(startDate)) &&
          (!endDate || new Date(row.timestamp) <= new Date(endDate))
      );
    setSearchResults(results);
  }, [searchTerm, startDate, endDate, dataLaporan]);

  // Fungsi untuk mengonversi data tabel menjadi format CSV
  // const convertToCSV = () => {
  //   const csvData = [["#", "Area", "Security", "Waktu"]];
  //   // UBAH DARI BACKEND
  //   searchResults.forEach((row) => {
  //     const rowData = [row.id, row.area_name, row.name, row.timestamp];
  //     csvData.push(rowData);
  //   });
  //   const csvContent = csvData.map((row) => row.join(",")).join("\n");
  //   return csvContent;
  // };
  const convertToCSV = () => {
    const csvData = [["#", "Area", "Security", "Waktu"]];
    searchResults.forEach((row, index) => {
      // Ubah format timestamp menjadi format yang diinginkan
      const formattedTimestamp = new Date(row.timestamp)
        .toLocaleString("en-US", {
          year: "numeric",
          month: "2-digit",
          day: "2-digit",
          hour: "2-digit",
          minute: "2-digit",
          second: "2-digit",
          hour12: true,
        })
        .replace(/,/g, ""); // Menghilangkan koma dari hasil
      const rowData = [index + 1, row.area_name, row.name, formattedTimestamp];
      csvData.push(rowData);
    });
    const csvContent = csvData.map((row) => row.join(",")).join("\n");
    return csvContent;
  };

  // Fungsi untuk menangani klik tombol download
  const handleDownload = () => {
    const csvContent = convertToCSV();
    const blob = new Blob([csvContent], { type: "text/csv;charset=utf-8" });
    saveAs(blob, "riwayat_patroli.csv");
  };

  return (
    <Layout>
      {role === "security" && (
        <h1 className=" sm:text-left mt-4 text-base">
          Hai <span className=" font-medium">{username}</span>
        </h1>
      )}
      <h1 className=" font-bold text-xl uppercase">Laporan Patroli</h1>
      <div className="my-4">
        {role === "admin" && (
          <div className="flex flex-col md:flex-row mb-4 md:gap-4">
            <label className="input input-bordered flex items-center gap-2">
              <input
                type="text"
                className="grow"
                placeholder="Search"
                value={searchTerm}
                onChange={(e) => setSearchTerm(e.target.value)}
              />
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 16 16"
                fill="currentColor"
                className="w-4 h-4 md:w-auto md:h-auto opacity-70"
              >
                <path
                  fillRule="evenodd"
                  d="M9.965 11.026a5 5 0 1 1 1.06-1.06l2.755 2.754a.75.75 0 1 1-1.06 1.06l-2.755-2.754ZM10.5 7a3.5 3.5 0 1 1-7 0 3.5 3.5 0 0 1 7 0Z"
                  clipRule="evenodd"
                />
              </svg>
            </label>
            <div className="flex flex-col md:flex-row md:gap-4">
              <label className="input input-bordered flex items-center gap-2">
                <input
                  type="date"
                  className="grow"
                  placeholder="Start Date"
                  value={startDate}
                  onChange={(e) => setStartDate(e.target.value)}
                />
              </label>
              <label className="input input-bordered flex items-center gap-2">
                <input
                  type="date"
                  className="grow"
                  placeholder="End Date"
                  value={endDate}
                  onChange={(e) => setEndDate(e.target.value)}
                />
              </label>
            </div>
            <button
              className="btn btn-primary mt-2 md:mt-0"
              onClick={handleDownload}
            >
              Download CSV
            </button>
          </div>
        )}
        {isLoading && (
          <div className=" flex w-full justify-center">
            <span className="loading loading-infinity loading-lg"></span>
          </div>
        )}
        <div className="overflow-x-auto my-4 z-0">
          <table className="table">
            {/* head */}
            <thead>
              <tr>
                <th>#</th>
                <th>Area</th>
                <th>Security</th>
                <th>Waktu</th>
              </tr>
            </thead>
            <tbody>
              {role === "admin" &&
                searchResults.map((row, index) => (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{row.area_name}</td>
                    <td>{row.name}</td>
                    <td>{row.timestamp}</td>
                  </tr>
                ))}
              {role === "security" &&
                dataLaporan.map((row, index) => (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{row.area_name}</td>
                    <td>{row.name}</td>
                    <td>{row.timestamp}</td>
                  </tr>
                ))}
            </tbody>
          </table>
        </div>
      </div>
    </Layout>
  );
};

export default checkAuth(HomeUser);
