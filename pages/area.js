import Layout from "@/components/layout";
import Image from "next/image";
import React, { useState, useRef, useEffect } from "react";
import { Modal } from "daisyui";
import Swal from "sweetalert2";
import Barcode from "react-barcode";
// import QRCode from "react-qr-code";
import QRCode from "qrcode.react";
import checkAuth from "@/middlewares/auth";
import API from "@/config/api";
import { format } from "date-fns";
const Area = () => {
  const [showModal, setShowModal] = useState(false);
  const [selectedImage, setSelectedImage] = useState("/logo-sarana.png");
  const [isLoading, setIsLoading] = useState(true);
  const [dataArea, setDataArea] = useState([]);
  const [idArea, setIdArea] = useState("");
  const [namaArea, setNamaArea] = useState("");
  const [deskripsi, setDeskripsi] = useState("");

  const fetchArea = async () => {
    setIsLoading(true);
    const token = localStorage.getItem("token");
    await API.get("/areas", {
      headers: {
        Authorization: `${token}`, // Sertakan token dalam header Authorization
      },
    })
      .then((data) => {
        console.log("DATA=>", data.data);
        setDataArea(data.data.data);
      })
      .catch((err) => console.log("Err", err));
    setIsLoading(false);
  };
  const handleImageClick = (imageUrl) => {
    document.getElementById("my_modal_2").showModal();
    setSelectedImage(imageUrl);
    setShowModal(true);
  };
  const handleDownload = () => {
    if (selectedImage) {
      const canvas = document.getElementById(selectedImage);
      const pngUrl = canvas
        .toDataURL("image/png")
        .replace("image/png", "image/octet-stream");
      let downloadLink = document.createElement("a");
      downloadLink.href = pngUrl;
      downloadLink.download = `Qrcode-Area.png`;
      // downloadLink.download = `${selectedImage}.png`;
      document.body.appendChild(downloadLink);
      downloadLink.click();
      document.body.removeChild(downloadLink);
    }
  };
  const handleGenerateBarcode = (id) => {
    Swal.fire({
      title: "Apakah anda yakin generate ulang Qrcode?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya",
    })
      .then((result) => {
        if (result.isConfirmed) {
          const randomString = generateRandomString(10);
          const token = localStorage.getItem("token");
          API.put(
            `/areas/${id}`,
            { barcode: randomString },
            {
              headers: {
                Authorization: `${token}`, // Sertakan token dalam header Authorization
              },
            }
          )
            .then((data) =>
              Swal.fire({
                title: "Qrcode Berhasil diperbarui!",
                // text: "Your file has been deleted.",
                icon: "success",
              })
            )
            .catch((err) =>
              Swal.fire({
                title: "Qrcode Gagal diperbarui!",
                // text: "Your file has been deleted.",
                icon: "error",
              })
            );
        }
        fetchArea();
      })
      .catch((err) => Swal.fire({ title: "Ulangi lagi", icon: "error" }));
  };
  const handleDelete = (id) => {
    Swal.fire({
      title: "Apakah anda yakin hapus area ini?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya",
    })
      .then((result) => {
        if (result.isConfirmed) {
          const token = localStorage.getItem("token");
          API.delete(`/areas/${id}`, {
            headers: {
              Authorization: `${token}`, // Sertakan token dalam header Authorization
            },
          })
            .then((data) =>
              Swal.fire({
                title: "Area berhasil dihapus!",
                // text: "Your file has been deleted.",
                icon: "success",
              })
            )
            .catch((err) =>
              Swal.fire({
                title: "Gagal!",
                // text: "Your file has been deleted.",
                icon: "error",
              })
            );
        }
      })
      .catch((err) => Swal.fire({ title: "Ulangi lagi", icon: "error" }));
    fetchArea();
  };
  const generateRandomString = (length) => {
    return [...Array(length)].map(() => Math.random().toString(36)[2]).join("");
  };
  const handleSubmit = async () => {
    const token = localStorage.getItem("token");
    const randomString = generateRandomString(10);
    console.log(randomString);
    const data = {
      area_name: namaArea,
      description: deskripsi,
      barcode: randomString,
    };
    document.getElementById("tambah_modal").close();
    await API.post("/areas", data, {
      headers: {
        Authorization: `${token}`, // Sertakan token dalam header Authorization
      },
    })
      .then((data) => {
        Swal.fire({
          title: "Area baru berhasil ditambahkan!",
          icon: "success",
        });
      })
      .catch((err) => Swal.fire({ title: "Gagal!", icon: "error" }));
    fetchArea();
  };
  useEffect(() => {
    fetchArea();
  }, []);
  const handleEdit = (id, name_area, desc) => {
    setIdArea(id);
    setNamaArea(name_area);
    setDeskripsi(desc);
    document.getElementById("tambah_modal").showModal();
  };
  const handleEditSubmit = async () => {
    const token = localStorage.getItem("token");
    const data = {
      area_name: namaArea,
      description: deskripsi,
    };
    await API.put(`/areas/${idArea}`, data, {
      headers: {
        Authorization: `${token}`, // Sertakan token dalam header Authorization
      },
    })
      .then((data) => {
        Swal.fire({
          title: "Area berhasil diupdate!",
          icon: "success",
        });
      })
      .catch((err) => Swal.fire({ title: "Gagal!", icon: "error" }));
    document.getElementById("tambah_modal").close();
    fetchArea();
  };
  return (
    <Layout>
      <h1 className=" font-bold text-xl uppercase">Manajemen Area</h1>
      <button
        className=" btn btn-primary my-4"
        onClick={() => {
          document.getElementById("tambah_modal").showModal();
          setIdArea("");
          setNamaArea("");
          setDeskripsi("");
        }}
      >
        Tambah Area
      </button>
      {/* TABLE */}

      <div className="overflow-x-auto mt-4">
        {isLoading ? (
          <div className=" w-full flex justify-center">
            <span className="loading loading-infinity loading-lg"></span>
          </div>
        ) : (
          <table className="table">
            <thead>
              <tr>
                <th></th>
                <th>Nama Area</th>
                <th>Deskripsi</th>
                <th>Barcode</th>
                <th>Tanggal Generate</th>
                <th>Aksi</th>
              </tr>
            </thead>

            <tbody>
              {dataArea.map((data, index) => (
                <tr>
                  <th>{index + 1}</th>
                  <td>{data.area_name}</td>
                  <td>{data.description}</td>
                  <td>
                    <QRCode
                      value={data.barcode}
                      // className=" w-2 h-2"
                      size={50}
                      onClick={() => handleImageClick(data.barcode)}
                    />
                  </td>
                  <td> {format(data.timestamp, "yyyy-MM-dd")}</td>
                  <td className=" flex gap-2  my-auto">
                    <div className="tooltip" data-tip="Generate Ulang Barcode">
                      <button
                        className=" btn btn-primary"
                        onClick={() => handleGenerateBarcode(data.id)}
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="16"
                          fill="currentColor"
                          className="bi bi-qr-code"
                          viewBox="0 0 16 16"
                        >
                          <path d="M2 2h2v2H2z" />
                          <path d="M6 0v6H0V0zM5 1H1v4h4zM4 12H2v2h2z" />
                          <path d="M6 10v6H0v-6zm-5 1v4h4v-4zm11-9h2v2h-2z" />
                          <path d="M10 0v6h6V0zm5 1v4h-4V1zM8 1V0h1v2H8v2H7V1zm0 5V4h1v2zM6 8V7h1V6h1v2h1V7h5v1h-4v1H7V8zm0 0v1H2V8H1v1H0V7h3v1zm10 1h-1V7h1zm-1 0h-1v2h2v-1h-1zm-4 0h2v1h-1v1h-1zm2 3v-1h-1v1h-1v1H9v1h3v-2zm0 0h3v1h-2v1h-1zm-4-1v1h1v-2H7v1z" />
                          <path d="M7 12h1v3h4v1H7zm9 2v2h-3v-1h2v-1z" />
                        </svg>
                      </button>
                    </div>
                    <div className="tooltip" data-tip="Edit Area">
                      <button
                        className=" btn btn-warning"
                        onClick={() =>
                          handleEdit(data.id, data.area_name, data.description)
                        }
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="16"
                          fill="currentColor"
                          className="bi bi-pencil-square"
                          viewBox="0 0 16 16"
                        >
                          <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                          <path
                            fillRule="evenodd"
                            d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5z"
                          />
                        </svg>
                      </button>
                    </div>
                    <div className="tooltip" data-tip="Delete Area">
                      <button
                        className=" btn btn-error"
                        onClick={() => handleDelete(data.id)}
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="16"
                          fill="currentColor"
                          className="bi bi-trash-fill"
                          viewBox="0 0 16 16"
                        >
                          <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5M8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5m3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0" />
                        </svg>
                      </button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        )}
      </div>

      {/* MODAL TAMBAH */}
      <dialog id="tambah_modal" className="modal">
        <div className="modal-box">
          <h3 className="font-bold  text-xl">
            {idArea > 0 ? "Edit" : "Tambah"} Area
          </h3>
          <label className="form-control w-full ">
            <div className="label">
              <span className="label-text">Nama Area</span>
            </div>
            <input
              type="text"
              placeholder="Area..."
              className="input input-bordered w-full "
              value={namaArea}
              onChange={(e) => setNamaArea(e.target.value)}
            />
          </label>
          <label className="form-control">
            <div className="label">
              <span className="label-text">Deskripsi</span>
            </div>
            <textarea
              className="textarea textarea-bordered h-24"
              placeholder="Deskripsi..."
              value={deskripsi}
              onChange={(e) => setDeskripsi(e.target.value)}
            ></textarea>
          </label>
          {idArea > 0 ? (
            <button
              className=" btn btn-accent w-full mt-4"
              onClick={handleEditSubmit}
            >
              Simpan
            </button>
          ) : (
            <button
              className=" btn btn-primary w-full mt-4"
              onClick={handleSubmit}
            >
              Simpan
            </button>
          )}
        </div>
        <form method="dialog" className="modal-backdrop">
          <button>close</button>
        </form>
      </dialog>
      {/* MODAL BARCODE */}
      <dialog id="my_modal_2" className="modal">
        <div className="modal-box text-center">
          <h3 className="font-bold text-center text-lg my-4">
            Pratinjau Barcode
          </h3>
          <div className=" flex w-full justify-center qrcode">
            {/* <QRCode value={selectedImage} ref={qrCodeCanvasRef} /> */}
            <QRCode
              id={selectedImage}
              value={selectedImage}
              size={290}
              level={"H"}
              includeMargin={true}
            />
          </div>
          {/* <Image
            src={selectedImage}
            className="mx-auto"
            width={250}
            height={250}
          /> */}
          <button
            className=" btn btn-primary w-full my-4"
            onClick={handleDownload}
          >
            Download
          </button>
        </div>
        <form method="dialog" className="modal-backdrop">
          <button>close</button>
        </form>
      </dialog>
    </Layout>
  );
};

export default checkAuth(Area);
