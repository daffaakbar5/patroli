import Layout from "@/components/layout";
import API from "@/config/api";
import checkAuth from "@/middlewares/auth";
import { format } from "date-fns";
import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";

const Security = () => {
  const [dataSecurity, setDataSecurity] = useState([]);
  const [name, setName] = useState("");
  const [dateofbirth, setDateofbirth] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [ubahPass, setUbahPass] = useState(false);
  const [newPassword, setNewPassword] = useState("");
  const [role, setRole] = useState("");
  const [idUser, setIdUser] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const fetchUsers = async () => {
    setIsLoading(true);
    const token = localStorage.getItem("token");
    await API.get("/users", {
      headers: {
        Authorization: `${token}`, // Sertakan token dalam header Authorization
      },
    })
      .then((data) => {
        console.log("DATA=>", data.data);
        setDataSecurity(data.data.data);
      })
      .catch((err) => console.log("Err", err));
    setIsLoading(false);
  };
  const handleSubmit = async () => {
    const token = localStorage.getItem("token");

    if (
      username === "" ||
      dateofbirth === "" ||
      name === "" ||
      password === "" ||
      role === ""
    ) {
      document.getElementById("tambah_modal").close();
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Harap isi semua kolom yang diperlukan!",
      });
      return;
    }

    const data = {
      name: name,
      dateofbirth: dateofbirth,
      username: username,
      password: password,
      role: role,
    };
    document.getElementById("tambah_modal").close();
    await API.post("/users", data, {
      headers: {
        Authorization: `${token}`, // Sertakan token dalam header Authorization
      },
    })
      .then((data) => {
        Swal.fire({
          title: "User baru berhasil ditambahkan!",
          icon: "success",
        });
      })
      .catch((err) => Swal.fire({ title: "Gagal!", icon: "error" }));
    fetchUsers();
  };
  const handleEdit = (id, name, username, dateofbirth, password, role) => {
    setIdUser(id);
    setName(name);
    setUsername(username);
    setDateofbirth(dateofbirth);
    setPassword(password);
    setRole(role);
    document.getElementById("tambah_modal").showModal();
  };
  useEffect(() => {
    fetchUsers();
  }, []);
  const handleEditSubmit = async () => {
    const token = localStorage.getItem("token");
    const data = {
      name: name,
      dateofbirth: dateofbirth,
      username: username,
      password: newPassword,
      role: role,
    };
    await API.put(`/users/${idUser}`, data, {
      headers: {
        Authorization: `${token}`, // Sertakan token dalam header Authorization
      },
    })
      .then((data) => {
        Swal.fire({
          title: "User berhasil diupdate!",
          icon: "success",
        });
      })
      .catch((err) => Swal.fire({ title: "Gagal!", icon: "error" }));
    document.getElementById("tambah_modal").close();
    fetchUsers();
  };
  const handleDelete = (id) => {
    setIsLoading(true);
    Swal.fire({
      title: "Apakah anda yakin hapus User ini?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya",
    })
      .then((result) => {
        if (result.isConfirmed) {
          const token = localStorage.getItem("token");
          API.delete(`/users/${id}`, {
            headers: {
              Authorization: `${token}`, // Sertakan token dalam header Authorization
            },
          })
            .then((data) => {
              Swal.fire({
                title: "User berhasil dihapus!",
                // text: "Your file has been deleted.",
                icon: "success",
              });
              fetchUsers();
            })
            .catch((err) =>
              Swal.fire({
                title: "Gagal!",
                // text: "Your file has been deleted.",
                icon: "error",
              })
            );
        }
      })
      .catch((err) => Swal.fire({ title: "Ulangi lagi", icon: "error" }));
    fetchUsers();
    setIsLoading(false);
  };
  return (
    <Layout>
      <h1 className=" font-bold text-xl uppercase">Manajemen Security</h1>
      <button
        className=" btn btn-primary my-4"
        onClick={() => {
          document.getElementById("tambah_modal").showModal();
          setIdUser("");
          setName("");
          setUsername("");
          setPassword("");
          setNewPassword("");
          setRole("");
          setDateofbirth("");
          setUbahPass(false);
        }}
      >
        Tambah Security
      </button>

      {/* TABEL */}
      <div className="overflow-x-auto mt-4">
        <table className="table">
          {/* head */}
          <thead>
            <tr>
              <th></th>
              <th>Nama</th>
              <th>Username</th>
              <th>Tanggal ditambahkan</th>
              <th>Role</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            {dataSecurity.map((data, index) => (
              <tr>
                <th>{index + 1}</th>
                <td>{data.name}</td>
                <td>{data.username}</td>
                <td>{format(data.createdAt, "yyyy-MM-dd")}</td>
                <td>{data.role}</td>
                <td className=" flex gap-2">
                  <div className="tooltip" data-tip="Edit User">
                    <button
                      className=" btn btn-warning"
                      onClick={() =>
                        handleEdit(
                          data.id,
                          data.name,
                          data.username,
                          data.dateofbirth,
                          data.password,
                          data.role
                        )
                      }
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="16"
                        height="16"
                        fill="currentColor"
                        className="bi bi-pencil-square"
                        viewBox="0 0 16 16"
                      >
                        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                        <path
                          fillRule="evenodd"
                          d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5z"
                        />
                      </svg>
                    </button>
                  </div>
                  <div className="tooltip" data-tip="Hapus User">
                    <button
                      className=" btn btn-error"
                      onClick={() => handleDelete(data.id)}
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="16"
                        height="16"
                        fill="currentColor"
                        className="bi bi-trash-fill"
                        viewBox="0 0 16 16"
                      >
                        <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5M8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5m3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0" />
                      </svg>
                    </button>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      {isLoading && (
        <div className=" flex w-full justify-center">
          <span className="loading loading-infinity loading-lg"></span>
        </div>
      )}
      {/* MODAL TAMBAH */}
      <dialog id="tambah_modal" className="modal">
        <div className="modal-box">
          <h3 className="font-bold  text-xl">
            {idUser > 0 ? "Edit" : "Tambah"} Security
          </h3>
          <label className="form-control w-full ">
            <div className="label">
              <span className="label-text">Nama</span>
            </div>
            <input
              type="text"
              placeholder="Nama..."
              className="input input-bordered w-full "
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </label>
          <label className="form-control w-full ">
            <div className="label">
              <span className="label-text">Tanggal lahir</span>
            </div>
            <input
              type="date"
              placeholder="DD MM YY..."
              className="input input-bordered w-full "
              value={dateofbirth}
              onChange={(e) => setDateofbirth(e.target.value)}
            />
          </label>
          <label className="form-control w-full ">
            <div className="label">
              <span className="label-text">Username</span>
            </div>
            <input
              type="text"
              placeholder="Username..."
              className="input input-bordered w-full "
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </label>
          {idUser > 0 && (
            <button
              className=" btn btn-primary mt-2"
              onClick={() => setUbahPass(true)}
            >
              Reset Password
            </button>
          )}
          {ubahPass && (
            <label className="form-control w-full ">
              <div className="label">
                <span className="label-text">Password Baru</span>
              </div>
              <input
                type="text"
                placeholder="Password..."
                className="input input-bordered w-full "
                value={newPassword}
                onChange={(e) => setNewPassword(e.target.value)}
              />
            </label>
          )}
          {idUser === "" && (
            <label className="form-control w-full ">
              <div className="label">
                <span className="label-text">Password</span>
              </div>
              <input
                type="text"
                placeholder="Password..."
                className="input input-bordered w-full "
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </label>
          )}
          <label className="form-control w-full ">
            <div className="label">
              <span className="label-text">Pilih Role</span>
            </div>
            <select
              className="select select-bordered"
              value={role}
              onChange={(e) => setRole(e.target.value)}
            >
              <option disabled selected>
                Pilih Role
              </option>
              <option value={"security"}>Security</option>
              <option value={"admin"}>Admin</option>
            </select>
          </label>

          {idUser > 0 ? (
            <button
              className=" btn btn-accent w-full mt-4"
              onClick={handleEditSubmit}
            >
              Simpan
            </button>
          ) : (
            <button
              className=" btn btn-primary w-full mt-4"
              onClick={handleSubmit}
            >
              Simpan
            </button>
          )}
        </div>
        <form method="dialog" className="modal-backdrop">
          <button>close</button>
        </form>
      </dialog>
    </Layout>
  );
};

export default checkAuth(Security);
