import Layout from "@/components/layout";
import React, { useEffect, useState } from "react";
import { QrReader } from "react-qr-reader";
import { format } from "date-fns";
import Swal from "sweetalert2";
import { useRouter } from "next/router";
import checkAuth from "@/middlewares/auth";
import { jwtDecode } from "jwt-decode";
import API from "@/config/api";

const ScanBarcode = (props) => {
  const [data, setData] = useState("No result");
  const [oke, setOke] = useState("");
  const navigate = useRouter();
  const [user, setUser] = useState([]);

  const handleSubmit = async () => {
    console.log("DATA=>", data);
    setOke(data);
    const token = localStorage.getItem("token");

    const decodedToken = jwtDecode(token);
    const id = decodedToken.id;
    await API.post(
      `/patrols`,
      { user_id: id, barcode: data },
      {
        headers: {
          Authorization: `${token}`, // Sertakan token dalam header Authorization
        },
      }
    )
      .then((data) => {
        console.log("DATA=>", data.data);
        // setDataArea(data.data.data);
        Swal.fire({
          title: "Success",
          text: `Qrcode berhasil ditambahkan!`,
          icon: "success",
        });
      })
      .catch((err) =>
        Swal.fire({
          title: "Error",
          text: `Qrcode Tidak sesuai!`,
          icon: "error",
        })
      );

    navigate.push("/home");
  };
  const [currentTime, setCurrentTime] = useState(new Date());
  const fetchUser = async () => {
    const token = localStorage.getItem("token");

    const decodedToken = jwtDecode(token);
    const id = decodedToken.id;
    await API.get(`/users/${id}`, {
      headers: {
        Authorization: `${token}`, // Sertakan token dalam header Authorization
      },
    })
      .then((data) => {
        console.log("DATA=>", data.data);
        // setDataArea(data.data.data);
        setUser(data.data.data);
      })
      .catch((err) => console.log("Err", err));
  };
  useEffect(() => {
    fetchUser();
  }, []);

  return (
    <Layout>
      {data !== "No result" ? (
        <>
          <h1>
            ID Barcode : <span className=" font-semibold">*************</span>
          </h1>
          <h1>
            Nama : <span className=" font-semibold"> {user.name} </span>
          </h1>
          <h1>
            Waktu :{" "}
            <span className=" font-semibold">
              {format(currentTime, "yyyy-MM-dd HH:mm:ss")}
            </span>
          </h1>
          <button
            className=" btn btn-primary w-full mt-4"
            onClick={handleSubmit}
          >
            Submit
          </button>
        </>
      ) : (
        <QrReader
          onResult={(result, error) => {
            if (!!result) {
              setData(result?.text);
            }

            if (!!error) {
              console.info(error);
            }
          }}
          constraints={{
            facingMode: "environment",
          }}
          style={{ width: "100%" }}
        />
      )}
    </Layout>
  );
};

export default checkAuth(ScanBarcode);
