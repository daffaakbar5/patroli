// import useStore from "@/store";
import useStorePatrol from "@/store";
import { jwtDecode } from "jwt-decode";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";

const Navbar = () => {
  const navigate = useRouter();
  // const { role } = useStorePatrol();
  const [role, setRole] = useState("");

  const handleLogout = () => {
    localStorage.removeItem("token");

    // Arahkan pengguna ke halaman login
    navigate.push("/");
  };
  useEffect(() => {
    const token = localStorage.getItem("token");
    const decodedToken = jwtDecode(token);
    const role = decodedToken.role;

    setRole(role);
  }, []);

  console.log("ROLEee->", role);
  return (
    <div className="drawer z-50">
      <input id="my-drawer-3" type="checkbox" className="drawer-toggle" />
      <div className="drawer-content flex flex-col">
        {/* Navbar */}
        <div className="w-full navbar bg-base-300 ">
          <div className="flex-none lg:hidden">
            <label
              htmlFor="my-drawer-3"
              aria-label="open sidebar"
              className="btn btn-square btn-ghost"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                className="inline-block w-6 h-6 stroke-current"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M4 6h16M4 12h16M4 18h16"
                ></path>
              </svg>
            </label>
          </div>
          <div className="flex-1 px-2 mx-2">Patrol Sarana</div>
          <div className="flex-none hidden lg:block">
            <ul className="menu menu-horizontal ">
              {/* Navbar menu content here */}
              <li>
                <a className="uppercase" onClick={() => navigate.push("home")}>
                  Home
                </a>
              </li>
              {role !== "admin" ? (
                <li>
                  <a
                    className="uppercase gap-4"
                    onClick={() => navigate.push("scan-barcode")}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="16"
                      height="16"
                      fill="currentColor"
                      className="bi bi-qr-code-scan"
                      viewBox="0 0 16 16"
                    >
                      <path d="M0 .5A.5.5 0 0 1 .5 0h3a.5.5 0 0 1 0 1H1v2.5a.5.5 0 0 1-1 0zm12 0a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0V1h-2.5a.5.5 0 0 1-.5-.5M.5 12a.5.5 0 0 1 .5.5V15h2.5a.5.5 0 0 1 0 1h-3a.5.5 0 0 1-.5-.5v-3a.5.5 0 0 1 .5-.5m15 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1 0-1H15v-2.5a.5.5 0 0 1 .5-.5M4 4h1v1H4z" />
                      <path d="M7 2H2v5h5zM3 3h3v3H3zm2 8H4v1h1z" />
                      <path d="M7 9H2v5h5zm-4 1h3v3H3zm8-6h1v1h-1z" />
                      <path d="M9 2h5v5H9zm1 1v3h3V3zM8 8v2h1v1H8v1h2v-2h1v2h1v-1h2v-1h-3V8zm2 2H9V9h1zm4 2h-1v1h-2v1h3zm-4 2v-1H8v1z" />
                      <path d="M12 9h2V8h-2z" />
                    </svg>
                    Scan barcode
                  </a>
                </li>
              ) : (
                <>
                  <li>
                    <a
                      className="uppercase gap-4"
                      onClick={() => navigate.push("area")}
                    >
                      Manajemen Area
                    </a>
                  </li>
                  <li>
                    <a
                      className="uppercase gap-4"
                      onClick={() => navigate.push("security")}
                    >
                      Manajemen Security
                    </a>
                  </li>
                </>
              )}
              <li>
                <a className="uppercase" onClick={handleLogout}>
                  Log Out
                </a>
              </li>
            </ul>
          </div>
        </div>
        {/* Page content here */}
        {/* Content */}
      </div>
      <div className="drawer-side">
        <label
          htmlFor="my-drawer-3"
          aria-label="close sidebar"
          className="drawer-overlay"
        ></label>
        <ul className="menu p-4 w-80 min-h-full bg-base-200">
          {/* Sidebar content here */}
          <li>
            <a
              className=" uppercase gap-4"
              onClick={() => navigate.push("home")}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="16"
                fill="currentColor"
                className="bi bi-house-fill"
                viewBox="0 0 16 16"
              >
                <path d="M8.707 1.5a1 1 0 0 0-1.414 0L.646 8.146a.5.5 0 0 0 .708.708L8 2.207l6.646 6.647a.5.5 0 0 0 .708-.708L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293z" />
                <path d="m8 3.293 6 6V13.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5V9.293z" />
              </svg>
              HOME
            </a>
          </li>
          {role !== "admin" ? (
            <li>
              <a
                className="uppercase gap-4"
                onClick={() => navigate.push("scan-barcode")}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  fill="currentColor"
                  className="bi bi-qr-code-scan"
                  viewBox="0 0 16 16"
                >
                  <path d="M0 .5A.5.5 0 0 1 .5 0h3a.5.5 0 0 1 0 1H1v2.5a.5.5 0 0 1-1 0zm12 0a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0V1h-2.5a.5.5 0 0 1-.5-.5M.5 12a.5.5 0 0 1 .5.5V15h2.5a.5.5 0 0 1 0 1h-3a.5.5 0 0 1-.5-.5v-3a.5.5 0 0 1 .5-.5m15 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1 0-1H15v-2.5a.5.5 0 0 1 .5-.5M4 4h1v1H4z" />
                  <path d="M7 2H2v5h5zM3 3h3v3H3zm2 8H4v1h1z" />
                  <path d="M7 9H2v5h5zm-4 1h3v3H3zm8-6h1v1h-1z" />
                  <path d="M9 2h5v5H9zm1 1v3h3V3zM8 8v2h1v1H8v1h2v-2h1v2h1v-1h2v-1h-3V8zm2 2H9V9h1zm4 2h-1v1h-2v1h3zm-4 2v-1H8v1z" />
                  <path d="M12 9h2V8h-2z" />
                </svg>
                Scan barcode
              </a>
            </li>
          ) : (
            <>
              <li>
                <a
                  className="uppercase gap-4"
                  onClick={() => navigate.push("area")}
                >
                  Manajemen Area
                </a>
              </li>
              <li>
                <a
                  className="uppercase gap-4"
                  onClick={() => navigate.push("security")}
                >
                  Manajemen Security
                </a>
              </li>
            </>
          )}
          <li>
            <a className="uppercase" onClick={handleLogout}>
              Log Out
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Navbar;
