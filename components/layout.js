import Navbar from "./Navbar";

export default function Layout({ children }) {
  return (
    <>
      <Navbar />
      <main className=" container mx-auto my-4">{children}</main>
    </>
  );
}
