// pages/middleware/authMiddleware.js

import { jwtDecode } from "jwt-decode";
import { useRouter } from "next/router";
import { useEffect } from "react";

const rolePermissions = {
  "/home": ["admin", "security"],
  "/area": ["admin"],
  "/security": ["admin"],
  "/scan-barcode": ["admin", "security"],
};

const checkAuth = (WrappedComponent) => {
  return (props) => {
    const router = useRouter();
    useEffect(() => {
      const token = localStorage.getItem("token");
      if (!token) {
        router.push("/");
      } else {
        const decodedToken = jwtDecode(token);
        const role = decodedToken.role;

        if (router.pathname === "/" && token) {
          router.push("/home");
        }
        const requiredRoles = rolePermissions[router.pathname];

        if (!requiredRoles || !requiredRoles.includes(role)) {
          router.push("/home");
        }
      }
    }, []);

    return <WrappedComponent {...props} />;
  };
};

export default checkAuth;
