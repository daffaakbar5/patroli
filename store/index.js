import { create } from "zustand";
const useStorePatrol = create((set) => ({
  role: "",
  updateRole: (newRole) => set({ role: newRole }),
}));

export default useStorePatrol;
