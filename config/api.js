import axios from "axios";

const BaseURL = "https://apipatrol.daffaakbar.my.id";
// const BaseURL = "http://localhost:4000";

const API = axios.create({
  baseURL: BaseURL,
});
export default API;
